import pytest
from django_features.models import ContentFeature
from django_features.utils import has_feature


class FakeContent(object):
    pass


@pytest.fixture
def basic_content_feature():
    return ContentFeature.objects.create(slug='basic', summary='basic')


@pytest.mark.django_db
def test_basic_content_feature(basic_content_feature):
    basic = ContentFeature.objects.get(slug='basic')
    assert basic.summary == 'basic'


@pytest.mark.django_db
def test_has_content_feature(basic_content_feature):
    with pytest.raises(NotImplementedError):
        has_feature(FakeContent(), 'basic')
