import pytest

from django.contrib.auth.models import Group

from django_features.models import Feature, State, FeatureLink


@pytest.fixture
def auth_group():
    group = Group(name='group')
    group.save()
    return group


@pytest.fixture
def group_state(auth_group):
    state = State(name='group-state', opt_in=False, public=False)
    state.save()
    state.add_group(auth_group)
    return state


@pytest.fixture
def group_feature(group_state):
    feature = Feature(slug='group_feature', summary='test feature',
                      state=group_state)
    FeatureLink(name='glob', feature=feature, url='https://foop.org/', text='')
    feature.save()
    return feature


@pytest.mark.django_db
def test_admin_root(admin_client):
    response = admin_client.get('/admin/django_features/')
    assert response.status_code == 200


@pytest.mark.django_db
def test_admin_states(admin_client):
    response = admin_client.get('/admin/django_features/state/')
    assert response.status_code == 200


@pytest.mark.django_db
def test_admin_state(admin_client, group_state):
    response = admin_client.get('/admin/django_features/state/{}/'.format(
        group_state.id))
    assert response.status_code != 404


@pytest.mark.django_db
def test_admin_feature(admin_client, group_feature):
    response = admin_client.get('/admin/django_features/feature/{}/'.format(
        group_feature.id))
    assert response.status_code != 404


@pytest.mark.django_db
def test_admin_features(admin_client, ):
    response = admin_client.get('/admin/django_features/feature/')
    assert response.status_code == 200
