import pytest
import mock

from django.db.utils import IntegrityError
from django.contrib.auth.models import AnonymousUser, User, Group
from hashlib import sha1

from django_features.models import (State, Feature, FeatureLink, FeatureFlag,
                                    is_authenticated_user)
from django_features.utils import (has_feature,
                                   FeatureSet, _local_cache,
                                   _user_attribute_name)


@pytest.fixture
def anonymous_user():
    return AnonymousUser()


@pytest.fixture
def auth_group():
    group = Group(name='group')
    group.save()
    return group


@pytest.fixture
def group_nonmember(auth_group):
    user = User(username='non-member')
    user.set_password('')
    user.save()
    return user


@pytest.fixture
def group_member(auth_group):
    user = User(username='member')
    user.set_password('')
    user.save()
    auth_group.user_set.add(user)
    return user


@pytest.fixture
def public_opt_in_state():
    state = State(name='public-opt-in-state', opt_in=True, public=True)
    state.save()
    return state


@pytest.fixture
def group_state(auth_group):
    state = State(name='group-state', opt_in=False, public=False)
    state.save()
    state.add_group(auth_group)
    return state


@pytest.fixture
def group_opt_in_state(auth_group):
    state = State(name='group-opt-in-state', opt_in=True, public=False)
    state.save()
    state.add_group(auth_group)
    return state


@pytest.fixture
def public_feature(public_state):
    feature = Feature(slug='public_feature', summary='test feature',
                      state=public_state)
    feature.save()
    return feature


@pytest.fixture
def public_opt_in_feature(public_opt_in_state):
    feature = Feature(slug='public_opt_in_feature', summary='test feature',
                      state=public_opt_in_state, )
    feature.save()
    return feature


@pytest.fixture
def public_partially_rolled_out_opt_in_feature(public_opt_in_state):
    feature = Feature(slug='public_partially_rolled_out_opt_in_feature',
                      partial_rollout=True,
                      percentage=100,
                      summary='test feature',
                      state=public_opt_in_state, )
    feature.save()
    return feature


@pytest.fixture
def group_feature(group_state):
    feature = Feature(slug='group_feature', summary='test feature',
                      state=group_state)
    feature.save()
    return feature


@pytest.fixture
def group_opt_in_feature(group_opt_in_state):
    feature = Feature(slug='group_opt_in_feature', summary='test feature',
                      state=group_opt_in_state)
    feature.save()
    return feature


@pytest.fixture
def users(anonymous_user, group_member, group_nonmember):
    return {
        'anonymous_user': anonymous_user,
        'group_member': group_member,
        'group_nonmember': group_nonmember
    }


@pytest.fixture
def features(public_feature, public_opt_in_feature,
             public_partially_rolled_out_opt_in_feature, group_feature,
             group_opt_in_feature):
    return {
        'public_feature': public_feature,
        'public_opt_in_feature': public_opt_in_feature,
        'public_partially_rolled_out_opt_in_feature': public_partially_rolled_out_opt_in_feature,
        'group_feature': group_feature,
        'group_opt_in_feature': group_opt_in_feature,
    }


@pytest.mark.django_db
def test_create_state():
    state = State(name='test-state')
    state.save()
    assert State.objects.get(name='test-state')


@pytest.mark.parametrize("public,opt_in,group,expected", [
    (False, False, False, Feature.Unavailable),
    (False, False, True, Feature.Unavailable),
    (False, True, False, Feature.Unavailable),
    (False, True, True, Feature.Unavailable),
    (True, False, False, None),
    (True, False, True, None),
    (True, True, False, Feature.Unavailable),
    (True, True, True, Feature.Unavailable),
])
@pytest.mark.django_db
def test_anonymous_access(public, opt_in, group, expected, anonymous_user,
                          auth_group):
    state = State(name='test-state', opt_in=opt_in, public=public)
    state.save()
    if group:
        state.add_group(auth_group)

    feature = Feature(slug='test-feature', summary='test feature', state=state)
    feature.save()

    if expected is None:
        assert isinstance(Feature.objects.get_if_available(
            anonymous_user,
            feature.slug), Feature)
    else:
        with pytest.raises(expected):
            Feature.objects.get_if_available(anonymous_user, feature.slug)


@pytest.mark.parametrize("public,opt_in,group,expected", [
    (False, False, False, Feature.Unavailable),
    (False, False, True, Feature.Unavailable),
    (False, True, False, Feature.Unavailable),
    (False, True, True, Feature.Unavailable),
    (True, False, False, None),
    (True, False, True, None),
    (True, True, False, None),
    (True, True, True, None),
])
@pytest.mark.django_db
def test_authenticated_access(public, opt_in, group, expected, group_nonmember,
                              auth_group):
    state = State(name='test-state', opt_in=opt_in, public=public)
    state.save()
    if group:
        state.add_group(auth_group)

    feature = Feature(slug='test-feature', summary='test feature', state=state)
    feature.save()

    if expected is None:
        assert isinstance(Feature.objects.get_if_available(
            group_nonmember,
            feature.slug), Feature)
    else:
        with pytest.raises(expected):
            Feature.objects.get_if_available(group_nonmember, feature.slug)


@pytest.mark.django_db
def test_multiple_user_instances(group_member):
    """
    Simulates multiple instances of a user within a single request.
    """
    _local_cache.featuresets = {}
    try:
        instance1 = User.objects.get(pk=group_member.id)
        FeatureSet.for_user(instance1)
        # simulate another part of the client code working with
        # another instance of the same user.
        instance2 = User.objects.get(pk=group_member.id)
        FeatureSet.for_user(instance2)
        FeatureSet.for_user(instance1).clear()
        assert getattr(instance1, _user_attribute_name, None) is None
        assert getattr(instance2, _user_attribute_name, None) is None
        # executing this should not raise an exception
        FeatureSet.for_user(instance2).clear()
    finally:
        _local_cache.featuresets = None


@pytest.mark.parametrize("public,opt_in,group,expected", [
    (False, False, False, Feature.Unavailable),
    (False, False, True, None),
    (False, True, False, Feature.Unavailable),
    (False, True, True, None),
    (True, False, False, None),
    (True, False, True, None),
    (True, True, False, None),
    (True, True, True, None),
])
@pytest.mark.django_db
def test_member_access(public, opt_in, group, expected, group_member,
                       auth_group):
    state = State(name='test-state', opt_in=opt_in, public=public)
    state.save()
    if group:
        state.add_group(auth_group)

    feature = Feature(slug='test-feature', summary='test feature', state=state)
    feature.save()

    if expected is None:
        assert isinstance(Feature.objects.get_if_available(
            group_member,
            feature.slug), Feature)
    else:
        with pytest.raises(expected):
            Feature.objects.get_if_available(group_member, feature.slug)


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'anonymous_user', True),
    ('public_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('group_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_feature', 'group_nonmember', True),
    ('public_opt_in_feature', 'group_nonmember', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', True),
    ('public_opt_in_feature', 'group_member', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', True),
    ('group_opt_in_feature', 'group_member', True),
])
@pytest.mark.django_db
def test_available_features(feature_name, user, expected, users, features):
    results = {feature.slug: feature for feature in
               Feature.objects.available_for_user(users[user])}
    assert (feature_name in results.keys()) == expected


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'anonymous_user', True),
    ('public_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('group_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_feature', 'group_nonmember', True),
    ('public_opt_in_feature', 'group_nonmember', False),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', True),
    ('public_opt_in_feature', 'group_member', False),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', True),
    ('group_opt_in_feature', 'group_member', False),
])
@pytest.mark.django_db
def test_feature_accessibility(feature_name, user, expected, users, features):
    result = Feature.objects.feature_enablement_for_user(users[user])
    assert result.get(feature_name, False) == expected
    assert features[feature_name].active(users[user]) == expected
    assert has_feature(users[user], feature_name) == expected
    assert bool(FeatureSet.for_user(users[user]).get(feature_name)) == expected
    if feature_name in FeatureSet.for_user(users[user]):
        assert bool(FeatureSet.for_user(users[user]).get(feature_name)) == expected


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'anonymous_user', False),
    ('public_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('group_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_feature', 'group_nonmember', True),
    ('public_opt_in_feature', 'group_nonmember', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', True),
    ('public_opt_in_feature', 'group_member', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', True),
    ('group_opt_in_feature', 'group_member', True),
])
@pytest.mark.django_db
def test_available_percentile_features(feature_name, user, expected, users, features):
    feature = features[feature_name]
    feature.percentage = 100
    feature.partial_rollout = True
    feature.save()
    results = {feature.slug: feature for feature in
               Feature.objects.available_for_user(users[user])}
    assert (feature_name in results.keys()) == expected


@pytest.mark.parametrize("feature_name,user,fully_rolled_out,expected", [
    ('public_feature', 'anonymous_user', True, False),
    ('public_opt_in_feature', 'anonymous_user', True, False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', True, False),
    ('group_feature', 'anonymous_user', True, False),
    ('group_opt_in_feature', 'anonymous_user', True, False),
    ('public_feature', 'group_nonmember', True, True),
    ('public_opt_in_feature', 'group_nonmember', True, False),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True, False),
    ('group_feature', 'group_nonmember', True, False),
    ('group_opt_in_feature', 'group_nonmember', True, False),
    ('public_feature', 'group_member', True, True),
    ('public_opt_in_feature', 'group_member', True, False),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True, False),
    ('group_feature', 'group_member', True, True),
    ('group_opt_in_feature', 'group_member', True, False),

    ('public_feature', 'anonymous_user', False, False),
    ('public_opt_in_feature', 'anonymous_user', False, False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False, False),
    ('group_feature', 'anonymous_user', False, False),
    ('group_opt_in_feature', 'anonymous_user', False, False),
    ('public_feature', 'group_nonmember', False, False),
    ('public_opt_in_feature', 'group_nonmember', False, True),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', False, True),
    ('group_feature', 'group_nonmember', False, False),
    ('group_opt_in_feature', 'group_nonmember', False, False),
    ('public_feature', 'group_member', False, False),
    ('public_opt_in_feature', 'group_member', False, True),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', False, True),
    ('group_feature', 'group_member', False, False),
    ('group_opt_in_feature', 'group_member', False, True),
])
@pytest.mark.django_db
def test_consideration_of_existing_flags_on_partial_rollout(feature_name, user, fully_rolled_out, expected, users, features):
    """ Pre-existing flags are the overriding determinant for feature visibility
    when feature is in an opt-in state.

    Therefore, as long as a feature is opt-in, activating a partial rollout
    on it should never disable the user's previous choice for a flag.

    :param feature_name:
    :param user:
    :param fully_rolled_out:
    :param expected:
    :param users:
    :param features:
    :return:
    """
    user_instance = users[user]
    feature = features[feature_name]
    if fully_rolled_out:
        percentage = 100
        feature.disable(user_instance)
    else:
        percentage = 0
        feature.enable(user_instance)

    feature.percentage = percentage
    feature.partial_rollout = True
    feature.save()
    results = Feature.objects.feature_enablement_for_user(user_instance)
    assert results.get(feature_name, False) == expected


@pytest.mark.parametrize("feature_name,user,expected_initial_visibility,expected_togglability", [
    ('public_feature', 'anonymous_user', False, False),
    ('group_feature', 'anonymous_user', False, False),

    ('public_feature', 'group_nonmember', False, True),
    ('group_feature', 'group_nonmember', False, False),

    ('public_feature', 'group_member', False, True),
    ('group_feature', 'group_member', False, True),
])
@pytest.mark.django_db
def test_unavailable_percentile_features(feature_name, user, expected_initial_visibility, expected_togglability, users, features):
    feature = features[feature_name]
    feature.percentage = 0
    feature.partial_rollout = True
    feature.save()
    results = {feature.slug: feature for feature in
               Feature.objects.available_for_user(users[user])}
    assert (feature_name in results.keys()) == expected_initial_visibility

    if not expected_initial_visibility:
        with pytest.raises(Feature.Unavailable):
            Feature.objects.get_if_available(
                users[user],
                features[feature_name]
            )

    assert has_feature(users[user], feature_name) is False
    assert (feature_name in FeatureSet.for_user(users[user])) == expected_initial_visibility

    assert not feature.state.opt_in
    # when the feature is not opt_in, calling enable should be a no-op
    feature.enable(users[user])
    assert has_feature(users[user], feature_name) is False
    assert (feature_name in FeatureSet.for_user(users[user])) == expected_initial_visibility

    feature.state.opt_in = True
    feature.state.save()

    # enable a partially rolled out feature after opt_in has been enabled
    feature.enable(users[user])

    if expected_togglability:
        assert has_feature(users[user], feature_name) is True
        assert (feature_name in FeatureSet.for_user(users[user])) != expected_initial_visibility
    else:
        assert has_feature(users[user], feature_name) is False
        assert (feature_name in FeatureSet.for_user(users[user])) == expected_initial_visibility


@pytest.mark.parametrize("feature_name,user", [
    ('public_opt_in_feature', 'anonymous_user'),
    ('group_opt_in_feature', 'anonymous_user'),

    ('public_opt_in_feature', 'group_nonmember'),
    ('group_opt_in_feature', 'group_nonmember'),

    ('public_opt_in_feature', 'group_member'),
    ('group_opt_in_feature', 'group_member'),
])
@pytest.mark.django_db
def test_included_by_partial_rollout(feature_name, user, users, features):
    """ After a user has set a flag on the feature, they should always have
    access to the feature, even when they are excluded from the partial
    rollout itself.
    """
    user_instance = users[user]
    feature = features[feature_name]
    feature.enable(user_instance)

    assert not feature.partial_rollout
    assert not (feature.excluded_by_partial_rollout(user_instance) or
                feature.included_by_partial_rollout(user_instance)), (
        "when a feature is not partially rolled out, the feature "
        "can neither include nor exclude a user")

    feature.partial_rollout = True
    feature.percentage = 0
    feature.save()

    while feature.percentage < 100:
        feature.percentage += 1
        feature.save()
        assert (feature.excluded_by_partial_rollout(user_instance) !=
                feature.included_by_partial_rollout(user_instance)), (
            "when partial rollout is activated, these should always "
            "return different values")


@pytest.mark.parametrize("feature_name,user,expected_availability,expected_enablement", [
    ('public_feature', 'anonymous_user', False, False),
    ('public_opt_in_feature', 'anonymous_user',  False, False),
    ('group_feature', 'anonymous_user', False, False),
    ('group_opt_in_feature', 'anonymous_user', False, False),

    ('public_feature', 'group_nonmember', True, True),
    ('public_opt_in_feature', 'group_nonmember', True, True),
    ('group_feature', 'group_nonmember', False, False),
    ('group_opt_in_feature', 'group_nonmember', False, False),

    ('public_feature', 'group_member', True, True),
    ('public_opt_in_feature', 'group_member', True, True),
    ('group_feature', 'group_member', True, True),
    ('group_opt_in_feature', 'group_member', True, True),
])
@pytest.mark.django_db
def test_flags_with_fully_available_percentile_features(feature_name, user, expected_availability, expected_enablement, users, features):
    feature = features[feature_name]
    user_instance = users[user]
    feature.percentage = 100
    feature.partial_rollout = True
    feature.save()

    results = {feature.slug: feature for feature in
               Feature.objects.available_for_user(user_instance)}
    assert (feature_name in results.keys()) == expected_availability
    assert has_feature(user_instance, feature_name) == expected_enablement
    assert (feature_name in FeatureSet.for_user(user_instance)) == expected_enablement

    feature.disable(user_instance)

    if feature.state.opt_in:
        assert (feature_name in results.keys()) == expected_availability
        assert has_feature(user_instance, feature_name) is False
        assert (feature_name in FeatureSet.for_user(user_instance)) == expected_availability
    else:
        assert (feature_name in results.keys()) == expected_availability
        assert has_feature(user_instance, feature_name) == expected_enablement
        assert (feature_name in FeatureSet.for_user(user_instance)) == expected_enablement


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_opt_in_feature', 'group_nonmember', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_opt_in_feature', 'group_member', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_opt_in_feature', 'group_member', True),
])
@pytest.mark.django_db
def test_opt_in_features(feature_name, user, expected, users, features):
    feature = features[feature_name]
    feature.enable(users[user])
    result = Feature.objects.feature_enablement_for_user(users[user])
    assert result.get(feature_name, False) == expected
    assert features[feature_name].user_enabled(users[user]) == expected
    assert has_feature(users[user], feature_name) == expected
    assert bool(FeatureSet.for_user(users[user]).get(feature_name)) == expected
    if feature_name in FeatureSet.for_user(users[user]):
        assert bool(FeatureSet.for_user(users[user]).get(feature_name)) == expected


@pytest.mark.parametrize("feature_name,user", [
    ('public_opt_in_feature', 'group_nonmember'),
    ('public_opt_in_feature', 'group_member'),
    ('public_partially_rolled_out_opt_in_feature', 'group_member'),
    ('group_opt_in_feature', 'group_member'),
])
@pytest.mark.django_db
def test_opt_out_features(feature_name, user, users, features):
    feature = features[feature_name]
    feature.enable(users[user])
    feature.disable(users[user])
    result = Feature.objects.feature_enablement_for_user(users[user])
    assert result[feature_name] is False
    assert features[feature_name].user_enabled(users[user]) is False
    assert has_feature(users[user], feature_name) is False
    assert feature_name in FeatureSet.for_user(users[user])
    assert FeatureSet.for_user(users[user])[feature_name] is False


@pytest.mark.django_db
def test_enabled_for_user(public_opt_in_feature, group_member, anonymous_user):
    feature = public_opt_in_feature
    assert not FeatureFlag.objects.disabled_for_user(group_member).filter(
        feature=feature).exists()
    assert not FeatureFlag.objects.enabled_for_user(group_member).filter(
        feature=feature).exists()
    public_opt_in_feature.enable(group_member)
    assert not FeatureFlag.objects.disabled_for_user(group_member).filter(
        feature=feature).exists()
    assert FeatureFlag.objects.enabled_for_user(group_member).filter(
        feature=feature).exists()
    public_opt_in_feature.disable(group_member)
    assert FeatureFlag.objects.disabled_for_user(group_member).filter(
        feature=feature).exists()
    assert not FeatureFlag.objects.enabled_for_user(group_member).filter(
        feature=feature).exists()
    public_opt_in_feature.enable(group_member)
    assert not FeatureFlag.objects.disabled_for_user(group_member).filter(
        feature=feature).exists()
    assert FeatureFlag.objects.enabled_for_user(group_member).filter(
        feature=feature).exists()

    public_opt_in_feature.enable(anonymous_user)
    assert not FeatureFlag.objects.enabled_for_user(anonymous_user).filter(
        feature=feature).exists()
    public_opt_in_feature.disable(anonymous_user)
    assert not FeatureFlag.objects.disabled_for_user(anonymous_user).filter(
        feature=feature).exists()


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'anonymous_user', False),
    ('public_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('group_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_feature', 'group_nonmember', False),
    ('public_opt_in_feature', 'group_nonmember', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', False),
    ('public_opt_in_feature', 'group_member', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', False),
    ('group_opt_in_feature', 'group_member', True),
])
@pytest.mark.django_db
def test_opt_in_features_list(feature_name, user, expected, users, features):
    results = {feature.slug: feature for feature in
               Feature.objects.available_for_user(users[user], optional_only=True)}
    assert (feature_name in results.keys()) == expected


@pytest.mark.django_db
def test_cache_clearing(group_nonmember, public_state):
    assert has_feature(group_nonmember, 'new-feature') is False
    Feature(slug='new-feature', state=public_state,
            summary='new feature').save()
    assert has_feature(group_nonmember, 'new-feature') is True


@pytest.mark.django_db
def test_unique_state_name():
    State.objects.create(name='foo')
    with pytest.raises(IntegrityError):
        State.objects.create(name='foo')


@pytest.mark.django_db
def test_unique_slug(public_state):
    Feature.objects.create(slug='foo', state=public_state)
    with pytest.raises(IntegrityError):
        Feature.objects.create(slug='foo', state=public_state)


@pytest.mark.django_db
def test_unique_link(public_state):
    feature = Feature.objects.create(slug='foo', state=public_state)
    FeatureLink.objects.create(feature=feature, name='blog',
                               url='http://foo.com/')
    with pytest.raises(IntegrityError):
        FeatureLink.objects.create(feature=feature, name='blog',
                                   url='http://foo.com/')


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'anonymous_user', True),
    ('public_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('group_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_feature', 'group_nonmember', True),
    ('public_opt_in_feature', 'group_nonmember', False),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', True),
    ('public_opt_in_feature', 'group_member', False),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', True),
    ('group_opt_in_feature', 'group_member', False),
])
@pytest.mark.django_db
def test_jinja_template(client, feature_name, user, expected, users, features):

    acct = users[user]
    if not acct.is_anonymous:
        client.login(username=acct.username, password='')
    response = client.get('/test_jinja')
    assert (feature_name in response.content.decode('utf-8')) == expected

@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'anonymous_user', True),
    ('public_opt_in_feature', 'anonymous_user', False),
    ('group_feature', 'anonymous_user', False),
    ('group_opt_in_feature', 'anonymous_user', False),
    ('public_partially_rolled_out_opt_in_feature', 'anonymous_user', False),
    ('public_feature', 'group_nonmember', True),
    ('public_opt_in_feature', 'group_nonmember', False),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', True),
    ('public_opt_in_feature', 'group_member', False),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', True),
    ('group_opt_in_feature', 'group_member', False),
])
@pytest.mark.django_db
def test_django_template(client, feature_name, user, expected, users, features):
    acct = users[user]
    if not acct.is_anonymous:
        client.login(username=acct.username, password='')
    response = client.get('/test_django')

    assert (feature_name in response.content[1:].decode('utf-8').split()) == expected


@pytest.mark.django_db
def test_django_cache(group_nonmember, public_feature):
    from django_features.utils import _user_attribute_name, _local_cache
    # clear threadlocal
    _local_cache.featuresets = None

    assert FeatureSet.for_user(group_nonmember).has_feature(public_feature.slug)

    # clear user attribute and rename underlying feature
    setattr(group_nonmember, _user_attribute_name, None)
    Feature.objects.filter(slug=public_feature.slug).update(
        slug='!' + public_feature.slug)

    # should still succeed
    assert FeatureSet.for_user(group_nonmember).has_feature(public_feature.slug)

    # clear cache
    FeatureSet.for_user(group_nonmember).clear()

    # should not succeed
    assert not FeatureSet.for_user(group_nonmember).has_feature(public_feature.slug)


@pytest.mark.django_db
def test_threadlocal_cache(group_nonmember, public_feature):
    from django_features.utils import _user_attribute_name, _local_cache
    # set threadlocal
    _local_cache.featuresets = {}

    featureset = FeatureSet.for_user(group_nonmember)
    assert featureset.has_feature(public_feature.slug)

    # clear user attribute and cache entry and rename underlying feature
    setattr(group_nonmember, _user_attribute_name, None)
    featureset.cache.delete(featureset.cache_key, version=1)
    Feature.objects.filter(slug=public_feature.slug).update(
        slug='!' + public_feature.slug)

    # should still succeed
    assert FeatureSet.for_user(group_nonmember).has_feature(public_feature.slug)

    # clear cache (including threadlocal)
    FeatureSet.for_user(group_nonmember).clear()

    # should not succeed
    assert not FeatureSet.for_user(group_nonmember).has_feature(public_feature.slug)


@pytest.mark.django_db
def test_threadlocal_cache_across_users(public_opt_in_feature, group_member,
                                        group_nonmember):
    from django_features.utils import _local_cache
    # set threadlocal
    _local_cache.featuresets = {}

    feature_slug = public_opt_in_feature.slug

    # Group membership is irrelevant to this test -- using these users because
    # they're here, but assigning to more distinct names for readability.
    bob = group_member
    alice = group_nonmember

    # Enabling feature for Bob shouldn't enable it for Alice
    public_opt_in_feature.enable(bob)
    assert FeatureSet.for_user(bob).has_feature(feature_slug)
    assert not FeatureSet.for_user(alice).has_feature(feature_slug)

    # Now make sure enabling for Alice actually works
    public_opt_in_feature.enable(alice)
    assert FeatureSet.for_user(alice).has_feature(feature_slug)

    # Disabling feature for Bob shouldn't disable it for Alice
    public_opt_in_feature.disable(bob)
    assert not FeatureSet.for_user(bob).has_feature(feature_slug)
    assert FeatureSet.for_user(alice).has_feature(feature_slug)


@pytest.mark.django_db
def test_clear_flags_on_user_delete(group_nonmember, public_opt_in_feature):
    public_opt_in_feature.enable(group_nonmember)
    user_id = group_nonmember.id

    assert FeatureFlag.objects.filter(user_id=user_id).exists()
    group_nonmember.delete()
    assert not FeatureFlag.objects.filter(user_id=user_id).exists()


@pytest.mark.django_db
def test_clear_group_on_group_delete(group_member, group_feature):
    group_member.groups.all().delete()
    assert not group_feature.state.groups.exists()


@pytest.mark.parametrize("feature_name,user,expected", [
    ('public_feature', 'group_nonmember', False),
    ('public_opt_in_feature', 'group_nonmember', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_nonmember', True),
    ('group_feature', 'group_nonmember', False),
    ('group_opt_in_feature', 'group_nonmember', False),
    ('public_feature', 'group_member', False),
    ('public_opt_in_feature', 'group_member', True),
    ('public_partially_rolled_out_opt_in_feature', 'group_member', True),
    ('group_feature', 'group_member', False),
    ('group_opt_in_feature', 'group_member', True),
])
@pytest.mark.django_db
def test_has_toggled_for_current_state(feature_name, user, expected, users, features):
    feature = features[feature_name]
    feature.enable(users[user])
    user_instance = users[user]
    assert expected == FeatureFlag.objects.filter(
        user=user_instance,
        feature__slug=feature_name,
        type=FeatureFlag.ENABLED,
    ).exists()

    feature.disable(user_instance, force=True)
    assert FeatureFlag.objects.filter(
        user=user_instance,
        feature__slug=feature_name,
        type=FeatureFlag.DISABLED,
    ).exists()


@pytest.mark.parametrize("feature_name", [
    'public_feature',
    'public_opt_in_feature',
    'public_partially_rolled_out_opt_in_feature',
    'group_feature',
    'group_opt_in_feature',
    'public_feature',
    'public_opt_in_feature',
    'public_partially_rolled_out_opt_in_feature',
    'group_feature',
    'group_opt_in_feature',
])
@pytest.mark.django_db
def test_pct_calculation(feature_name, features):
    class UserMock(object):
        def __init__(self, id):
            self.id = id
            self.pk = id

        @staticmethod
        def is_authenticated():
            return True

    feature = features[feature_name]

    # This method is copied from the code to make our
    # tests enforce the importance that this method's
    # behavior should not change, without careful
    # consideration of potential side effects.
    # See docstring on original method for details.
    def calculate_percentage_copy(feature, user):
        if not is_authenticated_user(user):
            return float("inf")
        sha = sha1()
        sha.update(feature.slug.encode('utf-8'))
        sha.update(str(user.id).encode('utf-8'))
        return int((ord(str(sha.digest())[0]) / 256.0) * 100)

    mock_users = [UserMock(user_id)
                  for user_id in range(0, 10 ** 17, 10 ** 15)]
    for u in mock_users:
        assert (calculate_percentage_copy(feature, u) ==
                feature._calculate_percentage(u))


@pytest.mark.django_db
def test_feature_link(public_feature):
    FeatureLink(name='blarg', feature=public_feature, text='',
                url='https://exmaple.com').save()

    assert 'blarg' in public_feature.links
