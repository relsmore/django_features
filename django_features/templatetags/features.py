from django.contrib.auth.models import User, AnonymousUser
from django.template import Library
from django_jinja import library as jinja
from jinja2.utils import contextfunction

from django_features.utils import has_feature

register = Library()

if not hasattr(jinja, 'global_function'):
    jinja = jinja.Library()


@jinja.global_function(name='has_feature')
@contextfunction
def jinja_has_feature(context, name, user=None):
    if not user:
        user = context['request'].user
    return has_feature(user, name)


@register.filter(name='has_feature')
def django_has_feature(request_or_user, name):
    if isinstance(request_or_user, (User, AnonymousUser)):
        return has_feature(request_or_user, name)
    return has_feature(request_or_user.user, name)
