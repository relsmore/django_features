from collections import namedtuple
from datetime import datetime, timedelta
from hashlib import sha1
import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models.signals import post_delete
from django.db.utils import IntegrityError
from django.utils.functional import cached_property

from django_features.expiration import check_expiration

logger = logging.getLogger(__name__)


def _upload_path(feature, filename):
    return '%s/%s/%s' % (
        getattr(settings, 'DJANGO_FEATURES_IMAGE_PATH', 'django_features'),
        feature.slug, filename)


class State(models.Model):
    name = models.CharField(max_length=256, blank=False, unique=True,
                            default=None, db_index=True)

    public = models.BooleanField(default=False, db_index=True)
    opt_in = models.BooleanField(default=False)

    label = models.CharField(max_length=256, blank=True)

    groups = models.ManyToManyField(Group, blank=True,
                                    through='StateGroupMembership')

    def add_group(self, group):
        try:
            StateGroupMembership.objects.create(group=group, state=self)
        except IntegrityError:
            # group already exists
            pass

    def remove_group(self, group):
        StateGroupMembership.objects.filter(state=self, group=group).delete()

    @cached_property
    def group_set(self):
        return set(self.groups.all())

    def has_groups(self, group_set):
        return bool(self.group_set.intersection(group_set))

    def is_available(self, group_set):
        """Returns  whether this state is available the the supplied groups

        If public or the specified groups match, then access should be granted.
        """
        return self.public or self.has_groups(group_set)

    def __unicode__(self):
        return self.name


class StateGroupMembership(models.Model):
    group = models.ForeignKey(Group, db_constraint=False,
                              on_delete=models.CASCADE)
    state = models.ForeignKey(State, on_delete=models.CASCADE)


def is_authenticated_user(user):
    return user and user.is_authenticated


FeatureFlagIds = namedtuple('FeatureFlagIds', ['enabled', 'disabled'])


class FeatureQuerySet(models.QuerySet):
    @staticmethod
    def _flagged_features_for_user(user):
        """ Returns the set of features for which the user has either opted
        in or opted out. This does not consider whether the features are
        opt-in.

        :param user:
        :return: FeatureFlagIds
        """
        enabled = set()
        disabled = set()

        if is_authenticated_user(user):
            active_user_flags = (FeatureFlag.objects
                                 .filter(user=user)
                                 .values_list('type', 'feature_id'))

            for flag in active_user_flags:
                if flag[0] == FeatureFlag.ENABLED:
                    enabled.add(flag[1])
                elif flag[0] == FeatureFlag.DISABLED:
                    disabled.add(flag[1])
                else:
                    raise Exception("Unhandled feature flag: %s" %
                                    flag[0])

        return FeatureFlagIds(enabled=enabled, disabled=disabled)

    @property
    def _base_qs(self):
        return self.select_related('state').prefetch_related('state__groups')

    def _for_user(self, user):
        """ QuerySet for the supplied user -- does not apply availability checks """
        if not is_authenticated_user(user):
            return self._base_qs.filter(
                state__opt_in=False, partial_rollout=False)
        else:
            return self._base_qs

    def get_if_available(self, user, slug):
        """ This method combines the retrieval of a Feature instance
        and the availability check for the given user.

        If the Feature does not exist, then a standard Django
        Feature.DoesNotExist exception is raised.

        If the feature is unavailable to the user, a
        Feature.Unavailable is raised.

        :param user:
        :param slug:
        :return:
        """
        try:
            feature = self._for_user(user).get(slug=slug)
            if not feature._is_available(user, user.groups.all()):
                raise Feature.Unavailable
            return feature
        except Feature.DoesNotExist:
            raise Feature.Unavailable

    def available_for_user(self, user, optional_only=False, promoted_only=False):
        """ When called with the default arguments, returns
        all the features that the user should either see in their
        feature management pane, or will have enabled for them by default.
        """

        if is_authenticated_user(user):
            user_groups = set(user.groups.all())
        else:
            user_groups = set()

        features_query = self._for_user(user)
        if optional_only:
            features_query = features_query.filter(state__opt_in=True)

        if promoted_only:
            features_query = features_query.filter(
                promote=True,
                promotion_date__lt=datetime.now()
            )

        return [f for f in features_query
                if f._is_available(user, user_groups)]

    def feature_enablement_for_user(self, user):
        """
        :param user:
        :return: A dict where the keys are all the available features
        for `user`, and the values are whether each feature should be
        enabled for `user`.
        """

        if is_authenticated_user(user):
            user_groups = set(user.groups.all())
            feature_flag_ids = self._flagged_features_for_user(user)
            user_enabled = feature_flag_ids.enabled
            user_disabled = feature_flag_ids.disabled
            user_flagged = user_enabled | user_disabled
        else:
            user_groups = set()
            user_enabled = set()
            user_disabled = set()
            user_flagged = set()

        available_features_for_user = {}

        for feature in self._for_user(user).all():
            check_expiration(feature)
            is_enabled_by_default = feature._is_enabled_by_default(
                user, user_groups)
            is_togglable = feature._is_togglable(user, user_groups)

            if not is_enabled_by_default and not is_togglable:
                continue

            if is_togglable and feature.id in user_flagged:
                if feature.id in user_enabled:
                    available_features_for_user[feature.slug] = True
                elif feature.id in user_disabled:
                    available_features_for_user[feature.slug] = False
            else:
                available_features_for_user[feature.slug] = is_enabled_by_default

        return available_features_for_user


def _default_expiration():
    return datetime.now() + timedelta(days=90)


class AbstractFeature(models.Model):
    slug = models.CharField(max_length=64, blank=False, db_index=True,
                            unique=True)
    summary = models.CharField(max_length=256, blank=False)
    created_on = models.DateTimeField(default=datetime.now)
    updated_on = models.DateTimeField(default=datetime.now)
    promote = models.BooleanField(
        default=False, help_text='Should the feature appear in ads and feeds')
    promotion_date = models.DateTimeField(
        default=datetime.now, help_text='Date when the feature goes live')
    description = models.TextField(
        blank=True, default="",
        help_text="Description of the feature (markdown supported)")
    screenshot = models.ImageField(upload_to=_upload_path, null=True,
                                   blank=True)
    tweet = models.CharField(max_length=140, blank=True, null=True)
    partial_rollout = models.BooleanField(
        default=False,
        help_text='This feature should only be made available '
                  'to a subset of users')
    percentage = models.PositiveIntegerField(default=0,
                                             help_text='Percentage of users')
    expiration_date = models.DateTimeField(
        blank=True,
        null=True,
        default=_default_expiration,
        help_text='The feature is not expected to be used after this date')
    owner = models.CharField(max_length=256, blank=True, null=True)
    owner_email = models.EmailField(max_length=256, blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ['-promotion_date']
        index_together = (('promote', 'promotion_date'),)

    class Unavailable(Exception):
        pass

    def __unicode__(self):
        return self.slug

    def save(self, **kwargs):
        self.updated_on = datetime.now()
        super(AbstractFeature, self).save(**kwargs)

    def _update_feature_state(self, user, flag_type, force):
        if not is_authenticated_user(user):
            return

        if force or self._is_togglable(user, user.groups.all()):
            try:
                flag = FeatureFlag.objects.get(feature=self, user=user)
            except FeatureFlag.DoesNotExist:
                try:
                    FeatureFlag(user=user,
                                feature=self,
                                type=flag_type).save()
                except IntegrityError:
                    pass
            else:
                if flag.type != flag_type:
                    # The post-save signal requires the exact user instance
                    # in order to clear the feature cache attached to it.
                    flag.user = user
                    flag.type = flag_type
                    flag.save()

    _links = None

    @property
    def links(self):
        if not self._links:
            links = {}
            for link in self.featurelink_set.all():
                links[link.name] = link
            self._links = links
        return self._links


class Feature(AbstractFeature):
    state = models.ForeignKey(State, db_index=True, on_delete=models.CASCADE)

    objects = FeatureQuerySet.as_manager()

    def enable(self, user, force=False):
        """ Set a feature flag to enable the feature for a user. By default the
        flag is only set if the user is allowed to toggle the feature (see
        _is_togglable). Pass in force=True to force the feature flag to be set
        even if the user cannot toggle the flag themselves.
        NOTE: the feature flag will only be honoured if the feature is in an
        opt-in state. The force option will not enable the feature for the user
        if the feature is not in an opt-in state, but it will ensure the feature
        is enabled for the user if it is later changed to an opt-in state.

        :param user: the user
        :param force: whether to force the feature flag to be set to enabled
        """
        self._update_feature_state(user, FeatureFlag.ENABLED, force)

    def disable(self, user, force=False):
        """ Set a feature flag to disable the feature for a user. By default the
        flag is only set if the user is allowed to toggle the feature (see
        _is_togglable). Pass in force=True to force the feature flag to be set
        even if the user cannot toggle the flag themselves.
        NOTE: the feature flag will only be honoured if the feature is in an
        opt-in state. The force option will not disable the feature for the user
        if the feature is not in an opt-in state, but it will ensure the feature
        is disabled for the user if it is later changed to an opt-in state.

        :param user: the user
        :param force: whether to force the feature flag to be set to disabled
        """
        self._update_feature_state(user, FeatureFlag.DISABLED, force)

    def user_enabled(self, user):
        """ This method tests a feature to see if the user has enabled it.
        It will return False if the user provided is None or Anonymous,
        if it is unavailable to the user or the feature is not enableable.

        :param user: the user to test or None
        :return: True if this feature has been enabled
        """
        if not self.state.opt_in:
            return False

        if not is_authenticated_user(user):
            return False

        if not FeatureFlag.objects.filter(
            feature=self,
            type=FeatureFlag.ENABLED,
            user=user,
        ).exists():
            return False

        return self._is_available(user, user.groups.all())

    def active(self, user):
        """ Returns True if this feature is active for this user

        This method tests a feature to see if it is active for a user. It will
        return False if the user provided is None or Anonymous and the feature
        is not public, if the feature is unavailable to the user or it isn't
        enabled.

        :param user: the user to test or None
        :return: True if this feature is active
        """
        if not self._is_available(user, user.groups.all()):
            return False

        if not self.state.opt_in:
            return True

        if user.is_authenticated:
            flag = FeatureFlag.objects.filter(feature=self, user=user)
            if flag:
                return flag.type == FeatureFlag.ENABLED

        return self._is_enabled_by_default(user, user.groups.all())

    def _is_togglable(self, user, group_set):
        if not self.state.opt_in:
            return False
        if not is_authenticated_user(user):
            return False
        if self.state.public or self.state.has_groups(group_set):
            return True

        return False

    def _is_available(self, user, group_set):
        """ Returns True if the user is allowed to toggle the feature or
        the feature should be enabled for the user by default.

        :param feature:
        :param user:
        :param group_set:
        :return:
        """
        if not self.state.is_available(group_set):
            return False

        if self.state.public and self.state.opt_in:
            return True

        if self.partial_rollout:
            return self.included_by_partial_rollout(user)
        return True

    def _calculate_percentage(self, user):
        """ XXX: Before you modify this method, know that doing so
        could break any ongoing partial rollouts. Hence, it is only
        safe to change this if there are no ongoing partial rollouts,
        or you continue to use the old logic on the ongoing rollouts,
        while applying the modified logic to new rollouts.

        :return: a value that will be compared to the feature's rollout
        percentage to determine whether a user is rolled in.
        """
        if not is_authenticated_user(user):
            # Denotes that this user should never be a member
            # of the partial rollout.
            return float("inf")
        sha = sha1()
        sha.update(self.slug.encode('utf-8'))
        sha.update(str(user.id).encode('utf-8'))
        return int((ord(str(sha.digest())[0]) / 256.0) * 100)

    def _is_enabled_by_default(self, user, group_set):
        """ If there were no feature flag present for the user and feature,
        would the feature be enabled for that user by default?

        :param feature:
        :param user:
        :param group_set:
        :return:
        """
        # Public, GA feature
        if (self.state.public and not (
                self.state.opt_in or self.partial_rollout)):
            return True

        # Partial-rollout
        if self.partial_rollout:
            if self.included_by_partial_rollout(user):
                if self.state.public:
                    return True
                elif self.state.has_groups(group_set):
                    return True
                else:
                    return False
            else:
                return False

        # Group membership
        if self.state.has_groups(group_set):
            if not self.state.opt_in:
                return True

        return False

    def excluded_by_partial_rollout(self, user):
        return self.partial_rollout and (
            self._calculate_percentage(user) >= self.percentage)

    def included_by_partial_rollout(self, user):
        return self.partial_rollout and (
            self._calculate_percentage(user) < self.percentage)


class ContentFeature(AbstractFeature):
    def active(self, content):
        """Is the feature active for this piece of content?
        """
        # This is will implemented in a follow-up PR. The idea is that content
        # features will have a list of rules that check the values of
        # attributes on the content object. These rules will be configurable
        # in the Django admin.
        raise NotImplementedError


_DEFAULT_LINKS = {
    'documentation': {
        'text': 'Documentation',
    },
    'blog': {
        'text': 'Read more',
    },
}


def _get_link_settings():
    return getattr(settings, 'DJANGO_FEATURES_LINKS', _DEFAULT_LINKS)


class AbstractFeatureLink(models.Model):
    @staticmethod
    def link_settings():
        return _get_link_settings()

    name = models.CharField(max_length=24, blank=False, db_index=True)

    url = models.URLField(max_length=256, blank=False, null=False)
    text = models.CharField(max_length=256, blank=False, null=False)
    title = models.CharField(max_length=256, blank=True, null=False)

    class Meta:
        abstract = True
        unique_together = (('name', 'feature',),)


class FeatureLink(AbstractFeatureLink):
    feature = models.ForeignKey(Feature, on_delete=models.CASCADE)


class ContentFeatureLink(AbstractFeatureLink):
    feature = models.ForeignKey(ContentFeature, on_delete=models.CASCADE,
        related_name='featurelink_set')


class FeatureFlagQuerySet(models.QuerySet):
    def enabled_for_user(self, user):
        if not is_authenticated_user(user):
            return FeatureFlag.objects.none()
        return self.filter(user=user, type=FeatureFlag.ENABLED)

    def disabled_for_user(self, user):
        if not is_authenticated_user(user):
            return FeatureFlag.objects.none()
        return self.filter(user=user, type=FeatureFlag.DISABLED)


class FeatureFlag(models.Model):
    ENABLED = 'enabled'
    DISABLED = 'disabled'

    feature = models.ForeignKey(Feature, db_index=True,
                                on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, db_index=True,
                             db_constraint=False, on_delete=models.CASCADE)
    type = models.CharField(max_length=12, choices=(('enabled', 'enabled'),
                                                    ('disabled', 'disabled')),
                            db_index=True)
    created_on = models.DateTimeField(default=datetime.now)
    objects = FeatureFlagQuerySet.as_manager()

    class Meta:
        unique_together = (('feature', 'user'),)


def _clear_user_data(sender, instance=None, **kwargs):
    if isinstance(instance, get_user_model()):
        FeatureFlag.objects.filter(user=instance).delete()


def _clear_group_from_state(sender, instance=None, **kwargs):
    if isinstance(instance, Group):
        StateGroupMembership.objects.filter(group=instance).delete()


post_delete.connect(_clear_user_data, sender=settings.AUTH_USER_MODEL,
                    dispatch_uid='clear_feature_data_on_user_delete')


post_delete.connect(_clear_group_from_state, sender=Group,
                    dispatch_uid='clear_group_from_state_on_delete')
