# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_features', '0003_add_generic_links'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feature',
            name='blog_url',
        ),
        migrations.RemoveField(
            model_name='feature',
            name='cta_title',
        ),
        migrations.RemoveField(
            model_name='feature',
            name='cta_url',
        ),
        migrations.RemoveField(
            model_name='feature',
            name='documentation_url',
        ),
        migrations.AlterField(
            model_name='featurelink',
            name='name',
            field=models.CharField(db_index=True, max_length=24, choices=[(b'blog', b'blog'), (b'documentation', b'documentation')]),
            preserve_default=True,
        ),
    ]
