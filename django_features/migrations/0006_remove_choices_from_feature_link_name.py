# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_features', '0005_add_feature_slug_constraint'),
    ]

    operations = [
        migrations.AlterField(
            model_name='featurelink',
            name='name',
            field=models.CharField(max_length=24, db_index=True),
            preserve_default=True,
        ),
    ]
