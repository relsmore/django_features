# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import django_features.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.CharField(max_length=64, db_index=True)),
                ('summary', models.CharField(max_length=256)),
                ('created_on', models.DateTimeField(default=datetime.datetime.now)),
                ('updated_on', models.DateTimeField(default=datetime.datetime.now)),
                ('promote', models.BooleanField(default=True, help_text=b'Should the feature appear in ads and feeds')),
                ('promotion_date', models.DateTimeField(default=datetime.datetime.now, help_text=b'Date when the feature goes live')),
                ('description', models.TextField(default=b'', help_text=b'Description of the feature (markdown supported)', blank=True)),
                ('screenshot', models.ImageField(null=True, upload_to=django_features.models._upload_path, blank=True)),
                ('blog_url', models.URLField(max_length=256, null=True, blank=True)),
                ('documentation_url', models.URLField(max_length=256, null=True, blank=True)),
                ('tweet', models.CharField(max_length=140, null=True, blank=True)),
                ('partial_rollout', models.BooleanField(default=False, help_text=b'This feature should only be made available to a subset of users')),
                ('percentage', models.PositiveIntegerField(default=0, help_text=b'Percentage of users')),
            ],
            options={
                'ordering': ['-promotion_date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FeatureFlag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(db_index=True, max_length=12, choices=[(b'enabled', b'enabled'), (b'disabled', b'disabled')])),
                ('created_on', models.DateTimeField(default=datetime.datetime.now)),
                ('feature', models.ForeignKey(to='django_features.Feature', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_constraint=False, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, unique=True, max_length=256, db_index=True)),
                ('public', models.BooleanField(default=False, db_index=True)),
                ('opt_in', models.BooleanField(default=False)),
                ('label', models.CharField(max_length=256, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StateGroupMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group', models.ForeignKey(to='auth.Group', db_constraint=False, on_delete=models.CASCADE)),
                ('state', models.ForeignKey(to='django_features.State', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='state',
            name='groups',
            field=models.ManyToManyField(to='auth.Group', through='django_features.StateGroupMembership', blank=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='featureflag',
            unique_together=set([('feature', 'user', 'type')]),
        ),
        migrations.AddField(
            model_name='feature',
            name='state',
            field=models.ForeignKey(to='django_features.State', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterIndexTogether(
            name='feature',
            index_together=set([('promote', 'promotion_date')]),
        ),
    ]
