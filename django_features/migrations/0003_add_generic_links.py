# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_features', '0002_add_cta_fields'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeatureLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=24, db_index=True)),
                ('url', models.URLField(max_length=256)),
                ('text', models.CharField(max_length=256)),
                ('title', models.CharField(max_length=256, blank=True)),
                ('feature', models.ForeignKey(to='django_features.Feature', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='featurelink',
            unique_together=set([('name', 'feature')]),
        ),
    ]
