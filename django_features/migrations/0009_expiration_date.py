# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django_features.models import _default_expiration


class Migration(migrations.Migration):

    dependencies = [
        ('django_features', '0008_change_feature_flag_unique_together'),
    ]

    operations = [
        migrations.AddField(
            model_name='feature',
            name='expiration_date',
            # Do not set a default when the field is added.
            # Existing features intentionally have a null expiration date.
            # A default for new features is added next.
            field=models.DateTimeField(
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name='feature',
            name='expiration_date',
            field=models.DateTimeField(
                blank=True,
                null=True,
                # Add a default for new features.
                default=_default_expiration,
                help_text='The feature is not expected to be used'
                          ' after this date'
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feature',
            name='owner',
            field=models.CharField(max_length=256, blank=True,
                                   null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feature',
            name='owner_email',
            field=models.EmailField(max_length=256, blank=True,
                                    null=True),
        ),
    ]
