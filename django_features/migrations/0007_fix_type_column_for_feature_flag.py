# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def convert_feature_flag_type(apps, schema_editor):
    FeatureFlag = apps.get_model("django_features", "FeatureFlag")
    db_alias = schema_editor.connection.alias
    FeatureFlag.objects.using(db_alias).filter(type="('enabled',)").update(
        type='enabled'
    )


def revert_feature_flag_type(apps, schema_editor):
    FeatureFlag = apps.get_model("django_features", "FeatureFlag")
    db_alias = schema_editor.connection.alias
    # there are no 'disabled' type flags prior to this migration,
    # and setting these to ('disabled',) would exceed the 12 char
    # limit on the column.
    FeatureFlag.objects.using(db_alias).filter(type='disabled').delete()
    FeatureFlag.objects.using(db_alias).filter(type='enabled').update(
            type="('enabled',)"
    )


class Migration(migrations.Migration):
    dependencies = [
        ('django_features', '0006_remove_choices_from_feature_link_name'),
    ]

    operations = [
        migrations.RunPython(convert_feature_flag_type,
                             revert_feature_flag_type),
    ]
