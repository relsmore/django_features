# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_features', '0004_remove_feature_urls'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feature',
            name='slug',
            field=models.CharField(unique=True, max_length=64, db_index=True),
            preserve_default=True,
        ),
    ]
