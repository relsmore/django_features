# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_features', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feature',
            name='cta_title',
            field=models.CharField(help_text=b'Call to action link title', max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feature',
            name='cta_url',
            field=models.URLField(help_text=b'Call to action URL', max_length=256, null=True, blank=True),
            preserve_default=True,
        ),
    ]
