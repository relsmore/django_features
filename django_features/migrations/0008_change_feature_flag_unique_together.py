# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def convert_beta_states_to_opt_in(apps, schema_editor):
    """ All beta features (intended to be used for partial rollout)
    will have `opt_in` enabled, so that users who are partially rolled
    in can opt out.
    """
    State = apps.get_model("django_features", "State")
    db_alias = schema_editor.connection.alias
    State.objects.using(db_alias).filter(
        name__in=('internal-beta', 'public-beta')
    ).update(
        opt_in=True
    )


def revert_beta_states_to_non_opt_in(apps, schema_editor):
    State = apps.get_model("django_features", "State")
    db_alias = schema_editor.connection.alias
    State.objects.using(db_alias).filter(
        name__in=('internal-beta', 'public-beta')
    ).update(
        opt_in=False
    )


class Migration(migrations.Migration):
    dependencies = [
        ('django_features', '0007_fix_type_column_for_feature_flag'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='featureflag',
            unique_together=set([('feature', 'user')]),
        ),
        migrations.RunPython(convert_beta_states_to_opt_in,
                             revert_beta_states_to_non_opt_in),
    ]
