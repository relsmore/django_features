import sys
from distutils.core import setup
from setuptools.command.test import test as TestCommand

requirements = open('requirements.txt').readlines()
test_requirements = open('test-requirements.txt').readlines()


def get_version():
    return open('version.txt', 'r').read().strip()


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        # By default, we only want to only run our tests (and not the tests
        # of every egg that ``setup.py test`` can place in our project).
        self.pytest_args = '-s tests'

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

setup(
    name='django_features',
    version=get_version(),
    url='https://bitbucket.org/bitbucket/django_features',
    license='Apache',
    author='Atlassian',
    author_email='bitbucket@atlassian.com',
    description='Feature release and selection library',
    packages=['django_features',
              'django_features.migrations',
              'django_features.templatetags'],
    install_requires=requirements,
    cmdclass={'test': PyTest},
)
