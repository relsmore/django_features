from django.shortcuts import render


def test_jinja_tag(request):
    return render(request, 'test_tag.jinja', {})


def test_django_tag(request):
    return render(request, 'test_tag.html', {})
