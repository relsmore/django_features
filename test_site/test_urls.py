from django.conf.urls import url
from django.contrib import admin

from test_site import test_views

urlpatterns = [
    url(r'^test_jinja$', test_views.test_jinja_tag),
    url(r'^test_django$', test_views.test_django_tag),
    url(r'^admin/', admin.site.urls),
]
