## Django Features ##

### What? ###

It's like waffle but can also be like a what's new page

#### What's new ####

    from django.features.models import Feature


    def view_features(request):
        features = Feature.objects.available_for_user(promoted_only=True)
        return render_to_response(
            request, { 'features': features }, 'whats-new.html')


#### Waffle ####
    from django_features.utils import has_feature

    def x(request):
        if not has_feature(request.user, 'my-feature'):
            return Http404


### Django Admin

![Admin root](docs/admin-root.png)

### Django Admin - State

![State form](docs/state-form.png)

### Django Admin - Feature

![Feature form](docs/feature-form.png)


### Owner and Expiration Date ###

After feature X has fully shipped, it is typical to remove calls to `has_feature(user, 'X')` along with the old code path.
Leaving the `has_feature` calls and old code paths is generally considered tech debt unless the feature is intended to be long-lived.
To make this tech debt visible, provide an `owner`, `owner_email`, and/or `expiration_date`. The default expiration date is
90 days after the feature creation timestamp. All fields are optional.

#### Default expiration logging ####

If `has_feature` is called for an expired feature, the default behavior is to log the expired feature. For example:

```
WARNING ... Feature 'new-tang-flavor' expired at 2017-08-17 00:00:00.
```

If `owner` and `owner_email` are provided:

```
WARNING ... Feature 'new-tang-flavor' expired at 2017-08-17 00:00:00. Contact owner: Neil Armstrong <na@nasa.gov>
```

To capture these log messages, add a handler for `django_features.models` logger. For example:

```
LOGGING['loggers']['django_features.models'] = {
    'handlers': ['console'],
    'level': 'DEBUG'
}
```

#### Customized expiration behavior ####

To override the default behavior for expired features:

1. Write a function that accepts a feature as its only argument.
2. Set `DJANGO_FEATURE_EXPIRED` to the string used to import that function.


For example, say you have a `system_alert(email, msg)` function:

```
# my_app/util.py
def send_expiration_notice(feature):
    email = u'%s <%s>' % (feature.owner, feature.owner_email)

    expiration_date = '{:%Y-%m-%d %H:%M:%S}'.format(feature.expiration_date)
    msg = u"Feature '%s' expired at %s." % (feature.slug, expiration_date)

    system_alert(email, msg)

---

# settings.py
DJANGO_FEATURE_EXPIRED = 'my_app.util.send_expiration_notice'
```



### Running tests ###

    pip install tox
    tox

To test just Django 1.7 (for example):

    tox -e django17

If you're using OSX and see an error like "ValueError: zlib is required" it's
related to installing Pillow. One way to resolve the issue is with Homebrew:

    brew install homebrew/dupes/zlib
    brew link zlib --force
